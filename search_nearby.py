#!/usr/bin/env python3
# -*- coding: ascii -*-

import sys
import googlemaps
import re
from getopt import *
from config import *
from private import *

gmaps = googlemaps.Client(key=GSERVER_KEY)

class GoogleServerError(Exception):
    """An error which Google Maps API endpoints emit."""
    pass


def search_places(query) -> 'results':
    """Search places on Google Maps which match the query."""
    try:
        p_resp = gmaps.places(query)
    except KeyboardInterrupt:
        print('{}: exiting...'.format(sys.argv[0]))
        sys.exit(0)

    if p_resp['status'] != 'OK':
        raise GoogleServerError(p_resp['status'])

    return p_resp['results']


def get_destinations(results=[], filename='') -> 'destinations':
    """Get destinations either from place results, a file, or both."""
    total_dests = []

    if results != []:
        total_dests.extend(
            [result['formatted_address'] for result in results])

    if filename != '':
        with open(filename, 'r') as f:
            destinations = [addr.strip() for addr in f
                if addr.strip() != '' and
                    re.match(ADDRESS_REGEX, addr) != None]
        total_dests.extend(list(set(destinations)))

    return total_dests


def filter_destinations(destinations, max_duration=25) -> ('addr', 'duration'):
    """Filter every destination based on the given max duration."""
    for destination in destinations:
        try:
            dm_resp = gmaps.distance_matrix(ORIGIN, destination,
                mode=DEFAULT_MODE)
        except KeyboardInterrupt:
            print('\n{}: exiting...'.format(sys.argv[0]))
            sys.exit(0)

        dest_data = dm_resp['rows'][0]['elements'][0]
        if dest_data['status'] == 'OK':
            if dest_data['duration']['value'] <= (max_duration * 60):
                yield (destination, dest_data['duration']['text'])
        else:
            if dest_data['status'] == 'ZERO_RESULTS' or \
                dest_data['status'] == 'NOT_FOUND':
                continue
            raise GoogleServerError(dest_data['status'])


def print_err_exit(err, action):
    print('{}: {} on {}: {}'.format(sys.argv[0],
        type(err).__name__, action, err))
    sys.exit(1)


def main(*args, **kwargs):
    query = kwargs.get('query') or ''
    filename = kwargs.get('filename') or ''
    max_duration = kwargs['max_duration']
    results = []

    print('QUERY = ({})'.format(query))
    print('ADDRESSES FILENAME = ({})'.format(filename))
    print('Seeking for destinations which take under',
        max_duration, 'minutes to reach...')

    if query != '':
        try:
            results = search_places(query)
        except (GoogleServerError, Exception) as err:
            print_err_exit(err, 'search')

    try:
        destinations = get_destinations(results=results, filename=filename)
    except IOError as err:
        print_err_exit(err, 'getting destinations')

    try:
        for addr, duration in filter_destinations(destinations, max_duration):
            print('\n[{}] takes about [{}]\n'.format(addr, duration))
    except (GoogleServerError, Exception) as err:
        print_err_exit(err, 'filter')


def usage_err():
    print('Usage: {} [-q "query"] [-f filename] ' +
        'max-duration(mins)'.format(sys.argv[0]))
    sys.exit(1)


if __name__ == '__main__':
    kwargs = {}

    parsed = getopt(sys.argv[1:], 'q:f:')
    optargs, args = parsed

    if optargs == [] or args == []:
        usage_err()

    for optarg in optargs:
        opt, arg = optarg
        if opt == '-q':
            kwargs['query'] = arg
        elif opt == '-f':
            kwargs['filename'] = arg
        else:
            usage_err()

    if kwargs.get('query') == None and kwargs.get('filename') == None:
        print('{}: query or filename must be specified.'.format(sys.argv[0]))
        sys.exit(1)

    if len(args) != 1:
        usage_err()

    try:
        kwargs['max_duration'] = int(args[0])
    except ValueError as err:
        print('{}: invalid max duration given.'.format(sys.argv[0]))
        sys.exit(1)

    main(**kwargs)
